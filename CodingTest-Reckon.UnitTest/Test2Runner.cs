﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodingTest_Reckon.UnitTest
{
    [TestClass]
    public class Test2Runner
    {
        const string textToSearch = "Peter told me (actually he slurrred) that peter the pickle piper piped a pitted pickle before he petered out. Phew!";

        [TestMethod]
        public void GivenTextToSearchWhenSubTextIsPeterThenShouldReturn1_43_98()
        {
            var test2= new Test2();
            var subText = "Peter";
            var expectedResult = string.Join(", ", new[] { 1, 43, 98 });

            var actualResult = test2.Find(textToSearch, subText);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void Given_peter_as_TextToSearchWhenSubTextIsPeterThenShouldReturn1_43_98()
        {
            var test2 = new Test2();
            var subText = "peter";
            var expectedResult = string.Join(", ", new[] { 1, 43, 98 });

            var actualResult = test2.Find(textToSearch, subText);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void GivenTextToSearchWhenSubTextIsPeterThenShouldReturn53_81()
        {
            var test2 = new Test2();
            var subText = "Pick";
            var expectedResult = string.Join(", ", new[] { 53,81 });

            var actualResult = test2.Find(textToSearch, subText);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void GivenTextToSearchWhenSubTextIsPeterThenShouldReturn53_60_66_74_81()
        {
            var test2 = new Test2();
            var subText = "Pi";
            var expectedResult = string.Join(", ", new[] { 53,60,66,74,81 });

            var actualResult = test2.Find(textToSearch, subText);

            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void GivenTextToSearchWhenSubTextIsPeterThenShouldReturnNoOutput()
        {
            var test2 = new Test2();
            var subText = "Z";
            var expectedResult = "<No Output>";

            var actualResult = test2.Find(textToSearch, subText);

            Assert.IsTrue(expectedResult.Equals(actualResult,StringComparison.OrdinalIgnoreCase));
        }
    }
}
