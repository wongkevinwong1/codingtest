﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingTest_Reckon
{
    public class Test1
    {
        public static void Run()
        {
            
            for (int i = 1; i <= 100; i++)
            {
                var result = i.ToString();
                bool isDividedBy3 = false;
                bool isDividedBy5 = false;

                if (i%3 == 0)
                {
                    isDividedBy3 = true;
                }

                if (i % 5 == 0)
                {
                    isDividedBy5 = true;
                }

                if (isDividedBy3) result = "Boss";
                if (isDividedBy5) result = "Hog";
                if (isDividedBy3 && isDividedBy5) result = "BossHog";

                Console.WriteLine(result);
            }
            
        }
    }
}
