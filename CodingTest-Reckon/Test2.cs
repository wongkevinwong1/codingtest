﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingTest_Reckon
{
    public class Test2
    {
        public string Find(string textToSearch, string subText)
        {
            var noOutputString = "<No Output>";
          

            if (subText.Length > textToSearch.Length)
                return noOutputString;

            var textToSearchArray = textToSearch.ToArray();
            var subTextArray = subText.ToArray();
            List<int> position = new List<int>();
            List<Boolean> searchMatchedResult = new List<bool>();

            for (int i = 0; i < textToSearchArray.Length - subTextArray.Length + 1; i++)
            {
                for (int j = i; j < subTextArray.Length + i; j++)
                {
                    searchMatchedResult.Add((textToSearchArray[j].ToString().Equals(subTextArray[j - i].ToString(), StringComparison.OrdinalIgnoreCase)));
                }
                if (!searchMatchedResult.Any(c => c == false))
                    position.Add(i + 1);
                searchMatchedResult.Clear();
            }

            if (!position.Any())
                return noOutputString;
            else
            {
                return string.Join(", ", position.ToArray());
            }

        }
    }
}
